-- Copyright 2023 Stanford University
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.

-- Legion Trace Optimizer
--
-- Inserts begin/end trace calls to control runtime tracing.

local ast = require("regent/ast")
local std = require("regent/std")
local report = require("common/report")
local data = require("common/data")

local c = std.c

local context = {}

function context:__index (field)
  local value = context [field]
  if value ~= nil then
    return value
  end
  error ("context has no field '" .. field .. "' (in lookup)", 2)
end

function context:__newindex (field, value)
  error ("context has no field '" .. field .. "' (in assignment)", 2)
end

function context:new_task_scope()
  local cx = {
    var_flows = data.newmap(),
    var_futures = data.newmap(),
  }
  return setmetatable(cx, context)
end

local trace_id = 1
function context:new_trace_id_scope()
  local cx = {
    next_trace_id = trace_id,
    var_flows = self.var_flows,
    var_futures = self.var_futures,
  }
  trace_id = trace_id + 1
  return setmetatable(cx, context)
end

function context.new_global_scope()
  local cx = {}
  return setmetatable(cx, context)
end

function context:get_flow(v)
  if not self.var_flows[v] then
    self.var_flows[v] = data.newmap()
  end
  return self.var_flows[v]
end

local function flow_empty()
  return terralib.newlist()
end

local function flow_future()
  local result = terralib.newlist()
  result:insert(true)
  return result
end

local function flow_var(v)
  local result = terralib.newlist()
  result:insert(v)
  return result
end

local function flow_future_into(cx, lhs) -- Unconditionally flow future into l-value
  for k, v in ipairs(lhs) do
    cx.var_flows[v] = true
  end
end

local function flow_nil()
  return terralib.newlist()
end

local function flow_value(cx, node)
  if node.value then
    if node.value:is(ast.typed.expr) then
      return var_flow_analysis_expr(cx, node.value)
    end
    local value = terralib.newlist()
    value:insert(node.value)
    return value
  end
  return terralib.newlist()
end

local function meet_flow(...)
  local flow = terralib.newlist()
  for _, a in ipairs({...}) do
    for _, v in ipairs(a) do
      flow:insert(v)
    end
  end
  return flow
end

local analyze_var_flow_expr = {

  [ast.typed.expr.RawContext]                 = flow_nil,
  [ast.typed.expr.RawPhysical]                = flow_nil,
  [ast.typed.expr.RawRuntime]                 = flow_nil,
  [ast.typed.expr.Ispace]                     = flow_nil,
  [ast.typed.expr.Region]                     = flow_nil,
  [ast.typed.expr.Partition]                  = flow_nil,
  [ast.typed.expr.PartitionEqual]             = flow_nil,
  [ast.typed.expr.PartitionByField]           = flow_nil,
  [ast.typed.expr.PartitionByRestriction]     = flow_nil,
  [ast.typed.expr.Image]                      = flow_nil,
  [ast.typed.expr.ImageByTask]                = flow_nil,
  [ast.typed.expr.Preimage]                   = flow_nil,
  [ast.typed.expr.CrossProduct]               = flow_nil,
  [ast.typed.expr.CrossProductArray]          = flow_nil,
  [ast.typed.expr.ListSlicePartition]         = flow_nil,
  [ast.typed.expr.ListDuplicatePartition]     = flow_nil,
  [ast.typed.expr.ListSliceCrossProduct]      = flow_nil,
  [ast.typed.expr.ListCrossProduct]           = flow_nil,
  [ast.typed.expr.ListCrossProductComplete]   = flow_nil,
  [ast.typed.expr.ListPhaseBarriers]          = flow_nil,
  [ast.typed.expr.DynamicCollective]          = flow_nil,
  [ast.typed.expr.Await]                      = flow_nil,
  [ast.typed.expr.Copy]                       = flow_nil,
  [ast.typed.expr.Acquire]                    = flow_nil,
  [ast.typed.expr.Release]                    = flow_nil,
  [ast.typed.expr.AttachHDF5]                 = flow_nil,
  [ast.typed.expr.DetachHDF5]                 = flow_nil,
  [ast.typed.expr.AllocateScratchFields]      = flow_nil,
  [ast.typed.expr.Deref]                      = flow_nil,
  [ast.typed.expr.FieldAccess]                = flow_nil,
  [ast.typed.expr.Cast]                       = flow_nil,
  [ast.typed.expr.Ctor]                       = flow_nil,
  [ast.typed.expr.RawFields]                  = flow_nil,
  [ast.typed.expr.RawTask]                    = flow_nil,
  [ast.typed.expr.Isnull]                     = flow_nil,
  [ast.typed.expr.Null]                       = flow_nil,
  [ast.typed.expr.ListInvert]                 = flow_nil,
  [ast.typed.expr.ListRange]                  = flow_nil,
  [ast.typed.expr.ListIspace]                 = flow_nil,
  [ast.typed.expr.WithScratchFields]          = flow_nil,
  [ast.typed.expr.RegionRoot]                 = flow_nil,
  [ast.typed.expr.AddressOf]                  = flow_nil,
  [ast.typed.expr.ParallelizerConstraint]     = flow_nil,
  [ast.typed.expr.Projection]                 = flow_nil,
  [ast.typed.expr.Internal]                   = flow_nil,

  [ast.typed.expr.Future]                     = flow_value,
  [ast.typed.expr.FutureGetResult]            = flow_value,
  [ast.typed.expr.ListFromElement]            = flow_value,
  [ast.typed.expr.Advance]                    = flow_value,
  [ast.typed.expr.DynamicCast]                = flow_value,
  [ast.typed.expr.StaticCast]                 = flow_value,
  [ast.typed.expr.UnsafeCast]                 = flow_value,
  [ast.typed.expr.RawValue]                   = flow_value,
  [ast.typed.expr.CtorListField]              = flow_value,
  [ast.typed.expr.CtorRecField]               = flow_value,
  [ast.typed.expr.ImportIspace]               = flow_value,
  [ast.typed.expr.ImportRegion]               = flow_value,
  [ast.typed.expr.ImportPartition]            = flow_value,
  [ast.typed.expr.ImportCrossProduct]         = flow_value,
  [ast.typed.expr.Global]                     = flow_value,
  [ast.typed.expr.Function]                   = flow_value,
  [ast.typed.expr.Condition]                  = flow_value,
  [ast.typed.expr.Fill]                       = flow_value,
  [ast.typed.expr.Adjust]                     = flow_value,
  [ast.typed.expr.Arrive]                     = flow_value,
  [ast.typed.expr.PhaseBarrier]               = flow_value,
  [ast.typed.expr.IndexAccess]                = flow_value,
  [ast.typed.expr.MethodCall]                 = flow_value,
  
  [ast.typed.expr.Constant] = function(cx, node)
    local value = terralib.newlist()
    value:insert(node.value)
    return value
  end,

  [ast.typed.expr.DynamicCollectiveGetResult]        = flow_future,
  [ast.typed.expr.RawFuture]                         = flow_future,

  [ast.typed.expr.ID] = function(cx, node)
    return flow_var(node.value)
  end,

  [ast.typed.expr.Call] = function(cx, node)
    if std.is_task(node.fn.value) and 
      not node.fn.value.is_local and
      node.expr_type ~= terralib.types.unit then
        return flow_future()
    end
    return flow_empty()
  end,

  [ast.typed.expr.Binary] = function(cx, node)
    return meet_flow(
      var_flow_analysis_expr(cx, node.lhs),
      var_flow_analysis_expr(cx, node.rhs)
    )
  end,

  [ast.typed.expr.Unary] = function(cx, node)
    return var_flow_analysis_expr(cx, node.rhs)
  end,

}

local apply_var_flow_analysis_expr = ast.make_single_dispatch(
  analyze_var_flow_expr,
  {})

function var_flow_analysis_expr(cx, node)
  return apply_var_flow_analysis_expr(cx)(node)
end

local function flow_stat_var(cx, node)
  if node.symbol["symbol_name"] ~= "__normalized_in_future_opt" then
    if node.value == false then
      cx.var_flows[node.symbol] = false
    else
      local value = var_flow_analysis_expr(cx, node.value)
      cx.var_flows[node.symbol] = value
    end
  end
end

local analyze_var_flow_stat = {

  [ast.typed.stat.Var]                                = flow_stat_var,

  [ast.typed.stat.IndexLaunchNum] = function(cx, node)
    local reduce_lhs = node.reduce_lhs and
        var_flow_analysis_expr(cx, node.reduce_lhs) or
        flow_empty()
    flow_future_into(cx, reduce_lhs)
  end,

  [ast.typed.stat.IndexLaunchList] = function(cx, node)
    local reduce_lhs = node.reduce_lhs and
        var_flow_analysis_expr(cx, node.reduce_lhs) or
        flow_empty()
    flow_future_into(cx, reduce_lhs)
  end,

  -- Ignoring Hack for assignment and reduce in optimize_futures
  [ast.typed.stat.Assignment] = function(cx, node)
    local lhs = var_flow_analysis_expr(cx, node.lhs)[1]
    if lhs ~= nil then 
      if cx.var_flows[lhs] == false then
        local rhs = var_flow_analysis_expr(cx, node.rhs)
        if rhs ~= nil then
          cx.var_flows[lhs] = rhs
        end
      else
        cx.var_flows[lhs] = true
      end
    else
    end
  end,

  [ast.typed.stat.Reduce] = function(cx, node)
    local lhs = var_flow_analysis_expr(cx, node.lhs)[1]
    if lhs ~= nil then 
      if cx.var_flows[lhs] == false then
        local rhs = var_flow_analysis_expr(cx, node.rhs)
        if rhs ~= nil then
          cx.var_flows[lhs] = rhs
        end
      else
        cx.var_flows[lhs] = true
      end
    else
    end
  end,

  [ast.typed.stat.ForNum] = function(cx, node)
    local symbol = node.symbol
    local values = terralib.newlist()
    for k, v in ipairs(node.values) do
      local value = var_flow_analysis_expr(cx, v)[1]
      if value ~= nil then
        values:insert(value)
      end
    end
    cx.var_flows[symbol] = values
    cx.var_futures[symbol] = true
  end,

  [ast.typed.stat]                  = function() return end,
}

local apply_var_flow_analysis_stat = ast.make_single_dispatch(
  analyze_var_flow_stat,
  {})

local function var_flow_analysis_stat(cx, node)
  return ast.map_stat_postorder(apply_var_flow_analysis_stat(cx), node)
end

local function compute_var_futures(cx)
  for k1, v1 in cx.var_flows:items() do
    if not cx.var_futures[k1] then
      if v1 == true then
        cx.var_futures[k1] = true
      elseif v1 ~= false then
        for k2, v2 in pairs(v1) do
          if cx.var_futures[v2] or v2 == true and not cx.var_futures[k1] then
            cx.var_futures[k1] = true
          end
        end
      end
    end
  end
end

local function always_true(cx, node)
  return {true, node}
end

local function always_true_expr(cx, node)
  if std.type_supports_privileges(std.as_read(node.expr_type)) then
    node:printpretty()
  end
  return {true, node}
end

local function always_false(cx, node)
  return {false, node}
end

local function unreachable(cx, node)
  assert(false, "unreachable")
end

local valid_trace = {

  -- Expressions:

  [ast.typed.expr.Call]                       = always_true_expr,
  [ast.typed.expr.IndexAccess]                = always_true_expr,
  [ast.typed.expr.MethodCall]                 = always_true_expr,
  [ast.typed.expr.RawContext]                 = always_true_expr,
  [ast.typed.expr.RawPhysical]                = always_true_expr,
  [ast.typed.expr.RawRuntime]                 = always_true_expr,
  [ast.typed.expr.Ispace]                     = always_true_expr,
  [ast.typed.expr.Region]                     = always_true_expr,
  [ast.typed.expr.Partition]                  = always_true_expr,
  [ast.typed.expr.PartitionEqual]             = always_true_expr,
  [ast.typed.expr.PartitionByField]           = always_true_expr,
  [ast.typed.expr.PartitionByRestriction]     = always_true_expr,
  [ast.typed.expr.Image]                      = always_true_expr,
  [ast.typed.expr.ImageByTask]                = always_true_expr,
  [ast.typed.expr.Preimage]                   = always_true_expr,
  [ast.typed.expr.CrossProduct]               = always_true_expr,
  [ast.typed.expr.CrossProductArray]          = always_true_expr,
  [ast.typed.expr.ListSlicePartition]         = always_true_expr,
  [ast.typed.expr.ListDuplicatePartition]     = always_true_expr,
  [ast.typed.expr.ListSliceCrossProduct]      = always_true_expr,
  [ast.typed.expr.ListCrossProduct]           = always_true_expr,
  [ast.typed.expr.ListCrossProductComplete]   = always_true_expr,
  [ast.typed.expr.ListPhaseBarriers]          = always_true_expr,
  [ast.typed.expr.PhaseBarrier]               = always_true_expr,
  [ast.typed.expr.DynamicCollective]          = always_true_expr,
  [ast.typed.expr.Adjust]                     = always_true_expr,
  [ast.typed.expr.Arrive]                     = always_true_expr,
  [ast.typed.expr.Await]                      = always_true_expr,
  [ast.typed.expr.Copy]                       = always_true_expr,
  [ast.typed.expr.Fill]                       = always_true_expr,
  [ast.typed.expr.Acquire]                    = always_true_expr,
  [ast.typed.expr.Release]                    = always_true_expr,
  [ast.typed.expr.AttachHDF5]                 = always_true_expr,
  [ast.typed.expr.DetachHDF5]                 = always_true_expr,
  [ast.typed.expr.AllocateScratchFields]      = always_true_expr,
  [ast.typed.expr.Condition]                  = always_true_expr,
  [ast.typed.expr.Deref]                      = always_true_expr,
  [ast.typed.expr.ImportIspace]               = always_true_expr,
  [ast.typed.expr.ImportRegion]               = always_true_expr,
  [ast.typed.expr.ImportPartition]            = always_true_expr,
  [ast.typed.expr.ImportCrossProduct]         = always_true_expr,
  [ast.typed.expr.ID]                         = always_true_expr,
  [ast.typed.expr.Constant]                   = always_true_expr,
  [ast.typed.expr.Global]                     = always_true_expr,
  [ast.typed.expr.Function]                   = always_true_expr,
  [ast.typed.expr.FieldAccess]                = always_true_expr,
  [ast.typed.expr.Cast]                       = always_true_expr,
  [ast.typed.expr.Ctor]                       = always_true_expr,
  [ast.typed.expr.CtorListField]              = always_true_expr,
  [ast.typed.expr.CtorRecField]               = always_true_expr,
  [ast.typed.expr.RawFields]                  = always_true_expr,
  [ast.typed.expr.RawFuture]                  = always_true_expr,
  [ast.typed.expr.RawTask]                    = always_true_expr,
  [ast.typed.expr.RawValue]                   = always_true_expr,
  [ast.typed.expr.Isnull]                     = always_true_expr,
  [ast.typed.expr.Null]                       = always_true_expr,
  [ast.typed.expr.DynamicCast]                = always_true_expr,
  [ast.typed.expr.StaticCast]                 = always_true_expr,
  [ast.typed.expr.UnsafeCast]                 = always_true_expr,
  [ast.typed.expr.ListInvert]                 = always_true_expr,
  [ast.typed.expr.ListRange]                  = always_true_expr,
  [ast.typed.expr.ListIspace]                 = always_true_expr,
  [ast.typed.expr.ListFromElement]            = always_true_expr,
  [ast.typed.expr.DynamicCollectiveGetResult] = always_true_expr,
  [ast.typed.expr.Advance]                    = always_true_expr,
  [ast.typed.expr.WithScratchFields]          = always_true_expr,
  [ast.typed.expr.RegionRoot]                 = always_true_expr,
  [ast.typed.expr.Unary]                      = always_true_expr,
  [ast.typed.expr.Binary]                     = always_true_expr,
  [ast.typed.expr.AddressOf]                  = always_true_expr,
  [ast.typed.expr.Future]                     = always_true_expr,
  [ast.typed.expr.ParallelizerConstraint]     = always_true_expr,
  [ast.typed.expr.Projection]                 = always_true_expr,
  [ast.typed.expr.FutureGetResult]            = always_true_expr,

  [ast.typed.expr.Internal]                   = unreachable,

  -- Statements:

  [ast.typed.stat.ForNum] = function(cx, node)
    for k, v in ipairs(node.values) do
      local value = var_flow_analysis_expr(cx, v)[1]
      if value ~= nil and cx.var_futures[value] then
        return always_false(cx, node)
      end
    end
    return always_true(cx, node)
  end,

  [ast.typed.stat.ForList]                    = always_true,
  [ast.typed.stat.Assignment]                 = always_true,
  [ast.typed.stat.MustEpoch]                  = always_true,
  [ast.typed.stat.RawDelete]                  = always_true,
  [ast.typed.stat.Fence]                      = always_true,
  [ast.typed.stat.ParallelizeWith]            = always_true,
  [ast.typed.stat.Block]                      = always_true,
  [ast.typed.stat.IndexLaunchList]            = always_true,
  [ast.typed.stat.IndexLaunchNum]             = always_true,
  [ast.typed.stat.Var]                        = always_true,
  [ast.typed.stat.VarUnpack]                  = always_true,
  [ast.typed.stat.Reduce]                     = always_true,
  [ast.typed.stat.Expr]                       = always_true,

  [ast.typed.stat.MapRegions]                 = always_false,
  [ast.typed.stat.UnmapRegions]               = always_false,
  [ast.typed.stat.Return]                     = always_false,
  [ast.typed.stat.Break]                      = always_false,
  [ast.typed.stat.If]                         = always_false,
  [ast.typed.stat.While]                      = always_false,
  [ast.typed.stat.Elseif]                     = always_false,
  [ast.typed.stat.ParallelPrefix]             = always_false,
  [ast.typed.stat.Repeat]                     = always_false,
  [ast.typed.stat.BeginTrace]                 = always_false,
  [ast.typed.stat.EndTrace]                   = always_false,

  [ast.typed.stat.Internal]                   = unreachable,
  [ast.typed.stat.ForNumVectorized]           = unreachable,
  [ast.typed.stat.ForListVectorized]          = unreachable,
}

local function all_with_provenance(acc, value)
  if not value[1] then
    return value
  end
  return acc
end

local analyze_valid_trace_node = ast.make_single_dispatch(valid_trace, {ast.typed.expr, ast.typed.stat})

local function analyze_valid_trace(cx, node)
  return ast.mapreduce_expr_stat_postorder(analyze_valid_trace_node(cx),
  all_with_provenance,
  node, {true})
end

local function check_trace(cx, block)
  local result, result_node = unpack(analyze_valid_trace(cx, block))
  if not result then
    return result_node
  end
end

local optimize_traces = {}

local function apply_tracing_block(cx, node, continuation)
  local report_fail = report.error
  local block = node.block
  
  if node.annotations.index_launch:is(ast.annotation.Demand) then
    if node.annotations.trace:is(ast.annotation.Demand) then
      report_fail(node, "conflicting annotations: cannot do __index_launch and __trace")
    end
    return node { block = block }
  end

  local result_node = check_trace(cx, block)
  if result_node ~= nil then
    if node.annotations.trace:is(ast.annotation.Demand) then
      report_fail(node, "not able to be traced")
    end
    return node { block = continuation(block, true) }
  end
  
  local cx_temp = cx:new_trace_id_scope()

  local trace_id = ast.typed.expr.Constant {
    value = cx_temp.next_trace_id,
    expr_type = c.legion_trace_id_t,
    annotations = ast.default_annotations(),
    span = node.span,
  }

  local stats = terralib.newlist()
  stats:insert(
    ast.typed.stat.BeginTrace {
      trace_id = trace_id,
      annotations = ast.default_annotations(),
      span = node.span,
  })
  stats:insert(
    ast.typed.stat.Block {
      block = block,
      annotations = ast.default_annotations(),
      span = node.span,
  })
  stats:insert(
    ast.typed.stat.EndTrace {
      trace_id = trace_id,
      annotations = ast.default_annotations(),
      span = node.span,
  })

  return node { block = block { stats = stats } }
end

local function pass_through(cx, node, continuation)
  return continuation(node, true)
end

local node_tracing = {
  [ast.typed.stat.While]     = apply_tracing_block,
  [ast.typed.stat.ForNum]    = apply_tracing_block,
  [ast.typed.stat.ForList]   = apply_tracing_block,
  [ast.typed.stat.Repeat]    = apply_tracing_block,
  [ast.typed.stat.Block]     = apply_tracing_block,
  [ast.typed.stat.MustEpoch] = apply_tracing_block,
  [ast.typed.stat]           = pass_through,
}

local apply_tracing_node = ast.make_single_dispatch(
  node_tracing,
  {},
  pass_through)

function optimize_traces.block(cx, block)
  return block { stats = ast.map_node_continuation(apply_tracing_node(cx), block.stats) }
end

function optimize_traces.top_task(cx, node)
  local cx = cx:new_task_scope()
  var_flow_analysis_stat(cx, node.body)
  compute_var_futures(cx)
  local body = node.body and optimize_traces.block(cx, node.body) or false
  return node { body = body }
end

function optimize_traces.top(cx, node)
  if node:is(ast.typed.top.Task) and not node.config_options.leaf then
    return optimize_traces.top_task(cx, node)
  end
    return node
end

function optimize_traces.entry(node)
  local cx = context.new_global_scope({})
  return optimize_traces.top(cx, node)
end

optimize_traces.pass_name = "optimize_traces"

return optimize_traces
