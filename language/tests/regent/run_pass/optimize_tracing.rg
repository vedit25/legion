-- Copyright 2023 Stanford University
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.

import "regent"

task inc(r : region(int), y : int)
where reads writes(r) do
  for x in r do
    @x += y
  end
end

task main()
  var r = region(ispace(ptr, 4), int)
  var x0 = dynamic_cast(ptr(int, r), 0)
  var x1 = dynamic_cast(ptr(int, r), 1)
  var x2 = dynamic_cast(ptr(int, r), 2)
  var x3 = dynamic_cast(ptr(int, r), 3)
  @x0 = 1
  @x1 = 2
  @x2 = 3
  @x3 = 5
  var num = 4
  var s = region(ispace(ptr, 4), int)
  var y0 = dynamic_cast(ptr(int, s), 0)
  var y1 = dynamic_cast(ptr(int, s), 1)
  var y2 = dynamic_cast(ptr(int, s), 2)
  var y3 = dynamic_cast(ptr(int, s), 3)
  @y0 = 1
  @y1 = 2
  @y2 = 3
  @y3 = 5
  var p = partition(equal, s, ispace(ptr, num) )

  var t = 0
  __demand(__trace)
  while t < 2 do
    inc(r, 10)
    t += 1
  end

  __demand(__trace)
  for i = 0, 3 do
    inc(r, 100)
  end

  __demand(__trace)
  do
    inc(r, 1000)
  end

  __demand(__trace)
  for j = 0, num do
    for i = 0, num do
      inc(p[i], 10)
    end
  end

  __demand(__trace)
  for i = 0, num do
    var c = 0
    inc(p[c], 10)
  end

  var d = 0
  __demand(__trace)
  for i = 0, num do
    inc(p[d], 10)
  end

  regentlib.assert(@x0 == 1321, "test failed")
  regentlib.assert(@x1 == 1322, "test failed")
  regentlib.assert(@x2 == 1323, "test failed")
  regentlib.assert(@x3 == 1325, "test failed")
  regentlib.assert(@y0 == 121, "test failed1")
  regentlib.assert(@y1 == 42, "test failed2")
  regentlib.assert(@y2 == 43, "test failed3")
  regentlib.assert(@y3 == 45, "test failed4")
  
end
regentlib.start(main)
