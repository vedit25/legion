import "regent"

-- Define a field space
fspace myFields {
  a : int,
  b : float,
  c : double
}

-- Create a region with this field space
region = region(ispace(ptr, 100), myFields)

task processAC(subregion : region(myFields))
where
  reads writes(subregion.a)
do
  for index in subregion do
    subregion[index].a = subregion[index].a + 1
  end
end

task main()
    processAC(region.{a})
end

regentlib.start(main)